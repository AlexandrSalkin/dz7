﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ДЗ7
{
    class Program
    {
        public static void Main(string[] args)
        {
            List<string> stroki = new List<string>();

            string file = @"C:\111.txt";
            
            using (StreamReader sr = new StreamReader(file, System.Text.Encoding.Default))
            {
                string stroka;
                while ((stroka = sr.ReadLine()) != null)
                {
                    string uslovie = "<a href=\"(.*?)\">";

                    foreach (var otsev in Regex.Matches(stroka, uslovie))
                    {
                        stroki.Add(Regex.Match(otsev.ToString(), uslovie).Groups[1].Value);
                    }

                    Console.WriteLine(string.Join("\n", stroki));
                }
            } 
        }
    }
}
